from flask.ext.wtf import Form
from wtforms import StringField, BooleanField, PasswordField
from wtforms.fields.html5 import DecimalField
from wtforms.validators import DataRequired

class LoginForm(Form):
    login = StringField('login', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    # openid = StringField('openid', validators=[DataRequired()])
    # remember_me = BooleanField('remember_me', default=False)


class SetAnswerForm(Form):
    task_id = StringField('task_id', validators=[DataRequired()])
    answer = StringField('answer', validators=[DataRequired()])



class SettingsForm(Form):
    tasks_per_page = DecimalField('tasks_per_page', validators=[])


class StringForm(Form):
    string_entry = StringField('string_entry', validators=[DataRequired()])
