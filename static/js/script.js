function check_answer(elem, id) {
  if (elem.value.length == 0) return;
  var xhr = new XMLHttpRequest();
  var request = "/check/" + id + "/" + elem.value;
  xhr.open('GET', request, true);
  xhr.onreadystatechange = function() {
    if (xhr.readyState != 4) return;
    var status = xhr.responseText;
    if (status == 'CORRECT') {
      elem.style['background-color'] = 'lime';
    }
    else if (status == 'WRONG') {
      elem.style['background-color'] = 'orange';
    }
  }

  xhr.send(null);
}


function countdown_loop()
{
    var now = new Date();
    var newYear = new Date("Jun,05,2016,10:00:00");
    var totalRemains = newYear.getTime() - now.getTime();
    if (totalRemains>1)
    {
        var RemainsSec = parseInt(totalRemains/1000);
        var RemainsFullDays = parseInt(RemainsSec / (24*60*60));
        var secInLastDay = RemainsSec - RemainsFullDays*24*3600;
        var RemainsFullHours = parseInt(secInLastDay/3600);
        if (RemainsFullHours<10){RemainsFullHours="0"+RemainsFullHours};
        var secInLastHour=secInLastDay-RemainsFullHours*3600;
        var RemainsMinutes=(parseInt(secInLastHour/60));
        if (RemainsMinutes<10){RemainsMinutes="0"+RemainsMinutes};
        var lastSec=secInLastHour-RemainsMinutes*60;
        if (lastSec<10){lastSec="0"+lastSec};
        document.getElementById("countdown").innerHTML = "<strong>"+RemainsFullDays+"</strong>"+" дней "+RemainsFullHours+" ч "+RemainsMinutes+" мин "+lastSec+" с"
        setTimeout("countdown_loop()",1000);
    }
    else {document.getElementById("countdown").innerHTML = "Время пришло!";}
}
