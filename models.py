from app import db
from datetime import datetime, timedelta
from config import EXP_FOR_LVLUP
from hashlib import md5
from random import sample



friends = db.Table('friends',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('friend_id', db.Integer, db.ForeignKey('user.id'))
)



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nickname = db.Column(db.String(64), index=True, unique=True)
    password = db.Column(db.String(64))
    last_seen = db.Column(db.DateTime)
    prototypes = db.relationship("UserPrototype", backref="solver", lazy="dynamic")
    level = db.Column(db.Integer)
    # experience = db.Column(db.Integer)
    friends = db.relationship('User',
        secondary = friends,
        primaryjoin = (friends.c.user_id == id),
        secondaryjoin = (friends.c.friend_id == id),
        backref = db.backref('friend_of', lazy = 'dynamic'),
        lazy = 'dynamic')
    tasks_per_page = db.Column(db.Integer)


    def is_admin(self):
        return self.id == 1


    def handle_answer(self, proto_id, is_correct):
        had_solved = False
        up = self.prototypes.filter_by(proto_id=proto_id).first()
        if up is None:
            up = UserPrototype(solved=is_correct, attempts=1, last_attempt=datetime.utcnow())
            up.proto = Prototype.query.get(proto_id)
            self.prototypes.append(up)
        else:
            had_solved = up.solved
            if not had_solved:
                up.solved = is_correct
                up.attempts += 1
            up.last_attempt = datetime.utcnow()
        if is_correct and not had_solved:
            self._check_for_level_up(up.proto)
        return self


    # def gain_experience(self, amount):
    #     max_level = max(EXP_FOR_LVLUP) + 1
    #     if self.level >= max_level:
    #         return self
    #     max_exp = EXP_FOR_LVLUP[self.level]
    #     exp = self.experience + amount
    #     if exp >= max_exp: # level up
    #         self.level += 1
    #         exp -= max_exp
    #     self.experience = exp
    #     return self


    def _check_for_level_up(self, proto):
        return # not used
        total_count = Prototype.query.filter_by(type=proto.type).count()
        solved_count = self.solved(proto.type).count()
        if total_count == solved_count:
            self.level += 1


    def solved(self, type): # TODO: Переписать запрос
        return Prototype.query.filter_by(type=type).join(UserPrototype, (UserPrototype.proto_id==Prototype.id)).filter(UserPrototype.user_id==self.id).filter(UserPrototype.solved==True)

    def attempted(self, type):
        return Prototype.query.filter_by(type=type).join(UserPrototype, (UserPrototype.proto_id==Prototype.id)).filter(UserPrototype.user_id==self.id).filter(UserPrototype.solved==False)

    def difficult(self, type, attempts):
        from sqlalchemy import desc
        return self.solved(type).filter(UserPrototype.attempts>attempts).order_by(desc(UserPrototype.attempts))

    def initialize(self):
        self.level = 1
        # self.experience = 0
        self.last_seen = datetime.utcnow()
        self.tasks_per_page = 0
        return self


    def save(self):
        db.session.add(self)
        db.session.commit()
        return self


    def avatar(self, size=80):
        x = self.nickname + "ege-game.ru"
        # if self.email:
        #     x = self.email
        return 'http://www.gravatar.com/avatar/%s?d=monsterid&s=%d' %\
            (md5(x.encode()).hexdigest(), size)


    def add_friend(self, user):
        if not self.is_friend(user):
            self.friends.append(user)
            return self

    def remove_friend(self, user):
        if self.is_friend(user):
            self.friends.remove(user)
            return self

    def is_friend(self, user):
        return self.friends.filter(friends.c.friend_id == user.id).count() > 0


    def is_online(self, minutes=5):
        edge = datetime.utcnow() - timedelta(minutes=minutes)
        return self.last_seen > edge


    def make_sample(self, li):
        n = self.tasks_per_page
        if n <= 0:
            return li
        if n > len(li):
            n = len(li)
        return sample(li, n)


    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return '<User %r>' % (self.nickname)



class Prototype(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer)
    text = db.Column(db.String(2048))
    answer = db.Column(db.Float)

    def __repr__(self):
        return '<Prototype %d.%d>' % (self.type, self.id)



class UserPrototype(db.Model):
    __tablename__ = 'user_prototype'
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True)
    proto_id = db.Column(db.Integer, db.ForeignKey('prototype.id'), primary_key=True)
    solved = db.Column(db.Boolean)
    attempts = db.Column(db.Integer)
    last_attempt = db.Column(db.DateTime)
    proto = db.relationship("Prototype", backref="solver_assocs")


    def __repr__(self):
        return '<UserPrototype %d-%d>' % (self.user_id, self.proto_id)



