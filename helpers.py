from hashlib import md5

def make_md5hash_with_salt(text, salt='ege-game.ru'):
    x = ''.join([text, salt])
    md5hash = md5(x.encode()).hexdigest()
    return md5hash
