from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user, current_user, login_required
from app import app, db, lm
from .forms import LoginForm, SetAnswerForm, SettingsForm, StringForm
from .models import Prototype, User, UserPrototype
import random
from datetime import datetime
from .emails import send_email
from .helpers import make_md5hash_with_salt


@app.route('/')
def index():
    t = random.randrange(1,12+1)
    amount = 1
    return random_type_amount(t, amount)


@app.route('/test')
@login_required
def random_test():
    tasks = []
    for i in range(1, 12+1):
        tasks.append(_random_type_amount(i, 1)[0])
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/<int:type>/all')
def all_type(type):
    tasks = Prototype.query.filter_by(type=type).all()
    return render_template('tasks.html',
        tasks=tasks,
        type=type)


@app.route('/<int:type>')
def random_type(type):
    return redirect(url_for('all_type', type=type))


@app.route('/<int:type>/all/<int:amount>')
def random_type_amount(type, amount):
    if amount <= 0:
        return redirect(url_for('all_type', type=type))
    tasks = _random_type_amount(type, amount)
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/<int:type>/unsolved')
@login_required
def unsolved(type):
    tasks = Prototype.query.filter_by(type=type).all()
    solved_tasks = g.user.solved(type).all()
    for x in solved_tasks:
        tasks.remove(x)
    return render_template('tasks.html',
        tasks=tasks)

@app.route('/<int:type>/unsolved/<int:amount>')
@login_required
def unsolved_amount(type, amount):
    if amount <= 0:
        return redirect(url_for('unsolved', type=type))
    tasks = Prototype.query.filter_by(type=type).all()
    solved_tasks = g.user.solved(type).all()
    for x in solved_tasks:
        tasks.remove(x)
    if len(tasks) > 0:
        if amount > len(tasks):
            amount = len(tasks)
        tasks = random.sample(tasks, amount)
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/<int:type>/attempted')
@login_required
def attempted(type):
    tasks = g.user.attempted(type).all()
    return render_template('tasks.html',
        tasks=tasks)

@app.route('/<int:type>/attempted/<int:amount>')
@login_required
def attempted_amount(type, amount):
    if amount <= 0:
        return redirect(url_for('attempted', type=type))
    tasks = g.user.attempted(type).all()
    if len(tasks) > 0:
        if amount > len(tasks):
            amount = len(tasks)
        tasks = random.sample(tasks, amount)
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/<int:type>/solved')
@login_required
def solved(type):
    tasks = g.user.solved(type).all()
    return render_template('tasks.html',
        tasks=tasks)

@app.route('/<int:type>/solved/<int:amount>')
@login_required
def solved_amount(type, amount):
    if amount <= 0:
        return redirect(url_for('solved', type=type))
    tasks = g.user.solved(type).all()
    if len(tasks) > 0:
        if amount > len(tasks):
            amount = len(tasks)
        tasks = random.sample(tasks, amount)
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/id/<int:proto_id>')
def proto_by_id(proto_id):
    tasks = [Prototype.query.get(proto_id)]
    return render_template('tasks.html',
        tasks=tasks)


@app.route('/<int:type>/difficult')
@login_required
def difficult(type):
    tasks = g.user.difficult(type, 1)
    render_template('tasks.html',
        tasks=tasks)


def _random_type_amount(type, amount):
    tasks = Prototype.query.filter_by(type=type)
    tasks = random.sample(list(tasks), amount)
    return tasks


@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = SettingsForm()
    if form.validate_on_submit():
        g.user.tasks_per_page = int(form.tasks_per_page.data)
        g.user.save()
        flash("Успешно!")
    return render_template('settings.html',
        form=form)


@app.route('/check/<int:id>/<answer>')
def check_answer(id, answer):
    task = Prototype.query.get(id)
    if task is None or task.answer == None:
        return 'ERROR: no answer'
    answer = answer.replace(',', '.')
    correct = task.answer == float(answer)
    if g.user is not None and g.user.is_authenticated():
        u = g.user.handle_answer(id, correct)
        u.save()
    if correct:
        return 'CORRECT'
    else:
        return 'WRONG'


@app.route('/login', methods=['GET', 'POST'])
def login():
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(nickname=form.login.data).first()
        if user is None:
            flash("Неверный логин. Обратите внимание, что логин чувствителен к регистру.")
            return redirect(url_for('login'))
        if user.password == make_md5hash_with_salt(form.password.data):
            login_user(user, remember=True)
            flash("Успешная авторизация.")
            return redirect(request.args.get('next') or url_for('index'))
        else:
            flash('Неверный пароль.')
    return render_template('login.html',
                           title='Авторизация',
                           form=form)


@app.route('/login/<nickname>', methods=['GET', 'POST'])
def forced_login(nickname):
    if not g.user.is_admin():
        flash("Доступ запрещен.")
        return redirect(url_for('index'))
    u = User.query.filter_by(nickname=nickname).first()
    if u:
        logout_user()
        login_user(u, remember=False)
        flash("Вход выполнен успешно!")
        return redirect(url_for('user', nickname=u.nickname))
    else:
        flash("Пользователя %s нет в базе."%login_form.string_entry.data)
        return redirect(url_for('admin'))



@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/user/<nickname>')
@login_required
def user(nickname):
    u = User.query.filter_by(nickname=nickname).first()
    if u is None:
        flash('Пользователя %s не существует.' % nickname)
        return redirect(url_for('index'))
    solved = {}
    total = {}
    attempted = {}
    solved[0] = u.prototypes.filter_by(solved=True).count()
    total[0] = Prototype.query.count()
    for i in range(1, 14+1):
        solved[i] = u.solved(i).count()
        total[i] = Prototype.query.filter_by(type=i).count()
        attempted[i] = u.attempted(i).count()
    return render_template('user.html',
        user=u,
        total=total,
        solved=solved,
        attempted=attempted)


@app.before_request
def before_request():
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.utcnow()
        g.user.save()


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))




@app.route('/admin', methods=['GET', 'POST'])
@login_required
def admin():
    if not g.user.is_admin():
        flash('Доступ запрещен.')
        return redirect(url_for('index'))
    answer_form = SetAnswerForm()
    if answer_form.validate_on_submit():
        p = Prototype.query.get(answer_form.task_id.data)
        if p is None:
            flash("Задания %d нет в базе."%answer_form.task_id.data)
            return redirect(url_for('admin'))
        old_answ = p.answer
        p.answer = answer_form.answer.data
        db.session.add(p)
        db.session.commit()
        flash("Ответ для задания %d изменен с %s на %s" %\
            (p.id, old_answ, p.answer))
        return redirect(url_for('admin'))
    string_form = StringForm()
    if string_form.validate_on_submit():
        u = User.query.filter_by(nickname=string_form.string_entry.data).first()
        if u:
            logout_user()
            login_user(u, remember=False)
            flash("Вход выполнен успешно!")
            return redirect(url_for('user', nickname=u.nickname))
        else:
            flash("Пользователя %s нет в базе."%login_form.string_entry.data)
            return redirect(url_for('admin'))
    return render_template('admin.html',
                           answer_form=answer_form,
                           string_form=string_form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if g.user is not None and g.user.is_authenticated():
        flash("У Вас уже есть учетная запись!")
        return redirect(url_for('user', nickname=g.user.nickname))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(nickname=form.login.data).first()
        if user is not None: # already exists
            flash("Пользователь уже существует.")
            return redirect(url_for('register'))
        user = User(nickname=form.login.data)
        user.password = make_md5hash_with_salt(form.password.data)
        user.initialize()
        user.save()
        flash("Пользователь создан успешно.")
        return redirect(url_for('login'))
    return render_template('register.html',
                           form=form)


@app.route('/users')
def users():
    users = User.query.order_by(User.nickname).all()
    return render_template('users.html',
                           users=users)


@app.route('/online')
def online():
    users = [ u for u in User.query.all() if u.is_online() ]
    return render_template('users.html',
                           users=users)


@app.route('/add_friend/<nickname>')
@login_required
def add_friend(nickname):
    f = User.query.filter_by(nickname=nickname).first()
    if f is None:
        flash('Пользователя %s не существует!'%nickname)
        return redirect('index')
    g.user.add_friend(f)
    g.user.save()
    flash('%s теперь у Вас в друзьях.'%nickname)
    return redirect(url_for('user', nickname=nickname))


@app.route('/remove_friend/<nickname>')
@login_required
def remove_friend(nickname):
    f = User.query.filter_by(nickname=nickname).first()
    if f is None:
        flash('Пользователя %s не существует!'%nickname)
        return redirect('index')
    g.user.remove_friend(f)
    g.user.save()
    flash('%s исключен из списка Ваших друзей.'%nickname)
    return redirect(url_for('user', nickname=nickname))


@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

